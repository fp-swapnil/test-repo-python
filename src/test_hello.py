from hello import add, minus


def test_add():
    assert add(2, 3) == 5

def test_minus():
    assert minus(3, 2) == 1